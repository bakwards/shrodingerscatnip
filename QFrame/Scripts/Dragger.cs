﻿using UnityEngine;
using System.Collections;

public class Dragger : MonoBehaviour {

	private QFrame frame;
	public Camera frameCamera;

    public bool lockX = false;
    public bool lockY = false;

	private bool doGameStart = true;
	
	private Vector3 screenPoint;
	private Vector3 offset;

	// Use this for initialization
	void Start () {
        refresh();
		frame.onFrameReset += refresh;
	}

    /// <summary>
    /// To be used by the editor
    /// </summary>
    public void refresh()
    {
        if (frame == null) frame = transform.parent.GetComponent<QFrame>();
		Vector3 newPos = frame.getStartPoint();
		newPos.z = transform.position.z;
		transform.position = newPos;
    }

	void OnMouseDrag() {
        if (frame.running())
		{
			Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
			Vector3 curPosition = frameCamera.ScreenToWorldPoint(curScreenPoint) + offset;
			transform.position = curPosition;
			frame.updatePotential(curPosition.x, curPosition.y);
        }
	}

    void OnMouseDown()
	{
		FindObjectOfType<ControlCanvasController>().HideTheTutorial();
		if(doGameStart){
			FindObjectOfType<GUIController>().lvlFinished = false;
			frame.start();
		}
		doGameStart = false;
		screenPoint = frameCamera.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - frameCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

//        if (!isReset)
//        {
//            frame.reset();
//			refresh();
//            isReset = true;
//        }
//        else
//        {
//            //frame.start();
//        }
    }

    void OnMouseUp()
    {
        //frame.stop();
    }
}
