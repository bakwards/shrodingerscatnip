﻿using UnityEngine;
using System.Collections;

public class ScreenFlasher : MonoBehaviour {

	public static ScreenFlasher instance
	{
		get
		{
			return FindObjectOfType<ScreenFlasher>();
		}
	}

	public CanvasGroup flash;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(flash.alpha > 0){
			flash.alpha -= Time.deltaTime*5;
		}
	}

	public void EngageFlash(){
		flash.alpha = 1;
	}
}
