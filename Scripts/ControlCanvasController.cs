﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlCanvasController : MonoBehaviour {

	public Image infidelityBar;
	public RectTransform xMeanIndicator;
	public CanvasGroup theTutorial;
	public GameObject helpButton;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
//		float stability = 1 - Mathf.InverseLerp( CatController.instance.lastThreshold, CatController.instance.cloneTreshold, CatController.instance.systemEnergy);
//		infidelityBar.fillAmount = stability;
//		infidelityBar.color = Color.Lerp(Color.red, Color.green, stability);
//		Vector2 newAnchor = Vector2.right * (0.5f + 0.5f * -CatController.rotationDeg * 1.05f);
//		xMeanIndicator.pivot = newAnchor;

	}

	public void HideTheTutorial(){
		if(theTutorial.alpha < 1) return;
		StartCoroutine("HideTutorial");
		helpButton.SetActive(true);
	}

	IEnumerator HideTutorial(){
		while(theTutorial.alpha > 0){
			theTutorial.alpha -= Time.deltaTime;
			yield return null;
		}
		theTutorial.alpha = 0;
	}
}
