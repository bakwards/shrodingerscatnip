﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatController : MonoBehaviour {

	private static CatController _instance;
	public static CatController instance
	{
		get
		{
			if(_instance == null) _instance = GameObject.FindObjectOfType<CatController>();
			return _instance;
		}
	}

	public QFrame qFrame;
	public static float maxSpeed = 40;
	public List<CatMovement> cats = new List<CatMovement>();
	public GameObject catPrefab;
    public GameObject deathSound;
    public GameObject milkSound;
	public static float speed;
	public static float infidelity;
	public static float rotationDeg;
	public float cloneTreshold = Mathf.NegativeInfinity;
	public float systemEnergy;
	public float lastThreshold;
	float coolDown = 0;
	public float collapseCooldown = 5;
	public Animator catGUI;
	public GameObject particlePrefab;

	// Use this for initialization
	void Start () {
		GameObject firstCat = Instantiate(catPrefab) as GameObject;
		cats.Add(firstCat.GetComponent<CatMovement>());
		CheckThreshold();
	}
	
	// Update is called once per frame
	void Update () {
		coolDown = coolDown > 0 ? coolDown - Time.deltaTime : 0;
		if(qFrame.running()){
			speed = coolDown > 0 ? 0 : Mathf.InverseLerp((float)qFrame.yMax(), (float)qFrame.yMin(), (float)qFrame.getCurrentPointPhysics().amp) * maxSpeed;
			infidelity = 1-(float)qFrame.fidelity();
			rotationDeg = -(float)qFrame.getXMean();

			if(CheckThreshold()){
				CloneCat();
			}
		}
		for(int i = 0; i < cats.Count; i++){
			cats[i].MoveCat(qFrame.running());
		}
		if(Input.GetKeyDown(KeyCode.Space)){
			CollapseCat();
			//CloneCat();
		}
		catGUI.transform.Rotate(Vector3.forward * rotationDeg * 10);
		//catGUI.transform.localScale = new Vector3(Mathf.Sign(rotationDeg), 1, 1);
		catGUI.speed = speed * 0.05f;
	}
	public void CloneCat(){
		if(cats.Count < 1) return;
		ScreenFlasher.instance.EngageFlash();
		int master = Random.Range(0, cats.Count-1);
		GameObject newCat = Instantiate(catPrefab, cats[master].transform.position, Quaternion.identity) as GameObject;
		newCat.GetComponent<CatMovement>().Instantiate(cats[master].direction, cats[master].newCat.transform.rotation);
		cats.Add(newCat.GetComponent<CatMovement>());
		
	}

	List<GameObject> DestructionArray;

	public void CollapseCat(){
		if(DestructionArray != null && DestructionArray.Count > 0) return;
		FindObjectOfType<GUIController>().observations++;
		coolDown = collapseCooldown;
		CatMovement actualCat = cats[Random.Range(0, cats.Count)];
		cats.Remove(actualCat);
		DestructionArray = new List<GameObject>();
		while(0 < cats.Count){
			DestructionArray.Add (cats[0].gameObject);
			cats.RemoveAt(0);
		}
		StartCoroutine("CatDestroyer");
		cats.Add(actualCat);
		qFrame.reset();
		cloneTreshold = Mathf.NegativeInfinity;
		CheckThreshold();
	}


	IEnumerator CatDestroyer(){
		while(DestructionArray.Count > 0){
			if(DestructionArray[0] != null){
	            Instantiate(deathSound, DestructionArray[0].transform.position, Quaternion.identity);
				Instantiate(particlePrefab, DestructionArray[0].transform.position, Quaternion.identity);
				Destroy(DestructionArray[0]);
			}
			DestructionArray.RemoveAt(0);
			DestructionArray.TrimExcess();
			yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
		}
	}

	bool CheckThreshold(){
		float amp = (float)qFrame.getCurrentPointPhysics().amp;
		float energyMean = (float)qFrame.getEnergyMean();
		systemEnergy = energyMean - amp;
		if(systemEnergy > cloneTreshold){
			lastThreshold = cloneTreshold;
			cloneTreshold = systemEnergy + 50;
			return true;
		}
		return false;
	}

	public void CatGotMilk(CatMovement cat){
		cats.Remove(cat);
		Destroy(cat.gameObject);
        Instantiate(milkSound, transform.position, Quaternion.identity);
		cats.TrimExcess();
		cloneTreshold -= 50;
        if (cats.Count == 0) {
			GUIController guiCont = GameObject.FindObjectOfType<GUIController>();
			guiCont.lvlFinished = true;
			//guiCont.gameObject.SetActive(false);
			Application.LoadLevelAdditive("Win");
        }
	}
	
	public static Vector2 Rotate(Vector2 v, float degrees) {
		float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
		float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
		
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (sin * tx) + (cos * ty);
		return v;
	}
}
