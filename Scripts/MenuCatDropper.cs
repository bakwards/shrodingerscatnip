﻿using UnityEngine;
using System.Collections;

public class MenuCatDropper : MonoBehaviour {

    public GameObject catFace;


	void Start () {
        StartCoroutine(spawnCat());
	}

    IEnumerator spawnCat()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.5F, 3F));    
            Instantiate(catFace, new Vector3(Random.Range(222.0F, 595.0F), 380, -60), Quaternion.identity);
        }
        
    }
}
