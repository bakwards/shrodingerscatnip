﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIController : MonoBehaviour {

    public Text catAmount;
    public Text timeText;
    public bool lvlFinished = true;

	public Image speedOmeter;
	public Image rightRotation;
	public Image leftRotation;

	public Text observationCount;
	int _observations;
	public int observations
	{
		get
		{
			return _observations;
		}
		set
		{
			_observations = value;
			observationCount.text = _observations.ToString("000000");
		}
	}

    private float timer;

	// Use this for initialization
	void Start () {
		observations = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (!lvlFinished) timer += Time.deltaTime;

        float minutesF = timer / 60;
        float secondsF = timer % 60;
        int minutes = (int)minutesF;
        int seconds = (int)secondsF;
        if (seconds < 10)
        {
            timeText.text = minutes.ToString() + ":0" + seconds.ToString();
        } else timeText.text = minutes.ToString() + ":" + seconds.ToString();        

        GameObject[] cats = GameObject.FindGameObjectsWithTag("Player");
        catAmount.text = cats.Length.ToString() + " X";

		speedOmeter.fillAmount = CatController.speed / CatController.maxSpeed;
		float rotation = CatController.rotationDeg;
		if(rotation >= 0){
			leftRotation.fillAmount = rotation;
			rightRotation.fillAmount = 0;
		} else {
			rightRotation.fillAmount = -rotation;
			leftRotation.fillAmount = 0;
		}
        
	}
}
