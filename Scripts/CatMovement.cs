﻿using UnityEngine;
using System.Collections;

public class CatMovement : MonoBehaviour {

	public Vector2 direction = Vector2.down;
	public float momentum = 0;
	public Animator animator;
	string currentAnimation;
	public Animator newCat;
	float currentSpeed;
    float meowTimer = 0;
    public GameObject[] meows;

    public void Instantiate(Vector2 initialDirection, Quaternion rotation){
		newCat.transform.rotation = rotation;
		direction = initialDirection;
	}

	public void MoveCat (bool move) {
		if(move){
			float infidelity = CatController.infidelity;
			currentSpeed = CatController.speed;
			float rotationDeg = CatController.rotationDeg;
			Vector2 uncertainty = new Vector2(Random.Range(-infidelity, infidelity), Random.Range(-infidelity, infidelity));
			momentum = momentum < 1 ? momentum + Time.deltaTime : 1;
			GetComponent<Rigidbody2D>().velocity = Vector2.Lerp(Vector2.zero, direction + uncertainty, momentum) * Mathf.Max (currentSpeed, Random.Range(0,infidelity) * CatController.maxSpeed);
			float newRotation = rotationDeg + Mathf.Sign(rotationDeg) * 2 * Random.Range(0,infidelity);
			direction = CatController.Rotate(direction, newRotation);
			newCat.transform.rotation *= Quaternion.Euler(Vector3.forward * newRotation);
		} else {
			momentum = momentum > 0 ? momentum - Time.deltaTime : 0;
			GetComponent<Rigidbody2D>().velocity = Vector2.Lerp(Vector2.zero, direction, momentum) * CatController.maxSpeed;
		}
		UpdateAnimation();
        Meow();
    }


	void UpdateAnimation(){
		float velocity = GetComponent<Rigidbody2D>().velocity.magnitude;
		if(velocity <= 0){
			return;
		}
		newCat.SetFloat("Speed", velocity);
		animator.speed = velocity * 0.05f;
		if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y)){
			if(Mathf.Sign(direction.x) == 1 && currentAnimation != "walk_right") CurrentAnimation("walk_right");
			else if (Mathf.Sign(direction.x) == -1 && currentAnimation != "walk_left") CurrentAnimation("walk_left");
		} else {
			if(Mathf.Sign(direction.y) == 1 && currentAnimation != "walk_up") CurrentAnimation("walk_up");
			else if (Mathf.Sign(direction.y) == -1 && currentAnimation != "walk_down") CurrentAnimation("walk_down");
		}
	}

	void CurrentAnimation(string anim){
		currentAnimation = anim;
		animator.SetTrigger(anim);
	}

    void Meow()
    {
        meowTimer -= Time.deltaTime;
        if (meowTimer < 0)
        {
            meowTimer = Random.Range(5f, 15f);
            GameObject sound;
            sound = (GameObject)Instantiate(meows[Random.Range(0, meows.Length)], transform.position, Quaternion.identity);
            Destroy(sound, 5);
        }
    }
}
