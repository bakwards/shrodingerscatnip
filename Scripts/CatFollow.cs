﻿using UnityEngine;
using System.Collections;

public class CatFollow : MonoBehaviour {

	float distance;
	public float minHeight = -200;
	public float maxHeight = -500;
	public float heightLimit = 0.56f;
	public float widthLimit = 0.9f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float z = CurrentDistance();
		Vector2 point = PointToFollow();
		float x = Mathf.Clamp(point.x,(maxHeight-z)*widthLimit, (maxHeight-z)*-widthLimit);
		float y = Mathf.Clamp(point.y, (maxHeight-z)*heightLimit, (maxHeight-z)*-heightLimit);
		Vector3 target = new Vector3(x, y, z);
		transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime);
	}

	Vector2 PointToFollow(){
		if(CatController.instance.cats.Count < 1) return Vector2.zero;
		Vector2 average = Vector2.zero;
		for(int i = 0; i < CatController.instance.cats.Count; i++){
			average += (Vector2)CatController.instance.cats[i].transform.position;
		}
		average /= CatController.instance.cats.Count;
		return average;
	}
	float CurrentDistance(){
		if(CatController.instance.cats.Count < 1) return maxHeight;
		Vector2 maxVector = CatController.instance.cats[0].transform.position;
		Vector2 minVector = CatController.instance.cats[0].transform.position;
		for(int i = 1; i < CatController.instance.cats.Count; i++){
			maxVector.x = Mathf.Max(maxVector.x, CatController.instance.cats[i].transform.position.x);
			maxVector.y = Mathf.Max(maxVector.y, CatController.instance.cats[i].transform.position.y);
			minVector.x = Mathf.Min(minVector.x, CatController.instance.cats[i].transform.position.x);
			minVector.y = Mathf.Min(minVector.y, CatController.instance.cats[i].transform.position.y);
		}
		return Mathf.Max (maxHeight, minHeight - Vector2.Distance(maxVector, minVector));
	}
}
